import { Component, OnInit, Input } from '@angular/core';
import { faUserGroup } from '@fortawesome/free-solid-svg-icons';
import { faHardDrive } from '@fortawesome/free-solid-svg-icons';
import { faClockRotateLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  faClockRotateLeft = faClockRotateLeft;
  faUserGroup = faUserGroup;
  faHardDrive = faHardDrive;
  @Input()sidenav: boolean=true;
  constructor() {}

  ngOnInit() {
  }
  
}
