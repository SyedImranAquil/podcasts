import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PodcastersComponent } from './podcasters/podcasters.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {path:'search', component:SearchComponent},
  {path:'', component:HomeComponent},
  {path:'podcasters',component:PodcastersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [SearchComponent, HomeComponent, PodcastersComponent]
