import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent implements OnInit {

  constructor() { }
  @Input()popular: boolean=true;
  ngOnInit(): void {
  }

}
