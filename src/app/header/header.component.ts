import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { faPodcast } from '@fortawesome/free-solid-svg-icons';
import { faDiamond } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  faBell = faBell;
  faPodcast = faPodcast;
  faDiamond = faDiamond;
  constructor() { }
  ngOnInit(): void {
  }
  sidenav: boolean =false;
  popular: boolean = false;


  @Output() sidenavEvent = new EventEmitter<any>();
  @Output() popularEvent = new EventEmitter<any>();
  @Output() turnOffPopularEvent = new EventEmitter<any>();

  toggleSidenav(){
    this.sidenavEvent.emit();
  }

  togglePopular(){
    this.popular = !this.popular
    this.popularEvent.emit();
  }
  popOff(){
    this.popular = false;
    this.turnOffPopularEvent.emit()
  }

}
