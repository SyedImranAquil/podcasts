import { Component, OnInit, EventEmitter } from '@angular/core';
//Test service for data sharing
import { DataService } from './data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Podcasts';
  //Sidenav controller
  sidenav = false;
  popular = false;
  receiveSidenav(){
    this.sidenav = !this.sidenav;
  }
  
  //PopularPage Controller
  receivePopular(){
    this.sidenav = false;
    this.popular = !this.popular;
  }
  turnPopularOff(){
    this.popular = false
  }
  
  //Testing service to share data between angular components.
  constructor(private data: DataService){ }
  message!:string;
  ngOnInit(){
    this.data.currentMessage.subscribe(message => this.message = message);
  }
  changeMessage(){
    this.message = ("This message has been received by home.")
  }

}

