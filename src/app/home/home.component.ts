import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  faSearch = faSearch;
  faPlay = faPlay;
  display = true;
  
  
  //Testing service to share data between angular components.
  constructor(private data: DataService){ }
  message!:string;
  ngOnInit(){
    this.data.currentMessage.subscribe(message => this.message = message);
  }

}
